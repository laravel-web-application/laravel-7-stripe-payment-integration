<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Stripe Payment Gateway Integration</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <style>
        .container {
            padding: 0.5%;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <pre id="token_response"></pre>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <button class="btn btn-primary btn-block" onclick="pay(10)">Pay $10</button>
        </div>
        <div class="col-md-4">
            <button class="btn btn-success btn-block" onclick="pay(50)">Pay $50</button>
        </div>
        <div class="col-md-4">
            <button class="btn btn-info btn-block" onclick="pay(100)">Pay $100</button>
        </div>
    </div>

    <h2>Test Stripe 3</h2>
    <div class="title m-b-md">
        Laravel + Stripe
    </div>

    <div class="links">
        <form action="/api/payment" method="POST">
            <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_ZPpwR3nTtCDPE5KyZOBcVM58"
                data-amount="50"
                data-name="Uzumaki Naruto"
                data-description="Test payment from hendisantika.com"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto"
                data-currency="USD">
            </script>
        </form>
    </div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function pay(amount) {
        var handler = StripeCheckout.configure({
            key: 'pk_test_ZPpwR3nTtCDPE5KyZOBcVM58', // your publisher key id
            locale: 'auto',
            token: function (token) {
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
                console.log('Token Created!!');
                console.log(token)
                $('#token_response').html(JSON.stringify(token));

                $.ajax({
                    url: '{{ url("store") }}',
                    method: 'post',
                    data: {tokenId: token.id, amount: amount},
                    success: (response) => {

                        console.log(response)

                    },
                    error: (error) => {
                        console.log(error);
                        alert('Oops! Something went wrong')
                    }
                })
            }
        });

        handler.open({
            name: 'Demo Site',
            description: '2 widgets',
            amount: amount * 100
        });
    }
</script>
</body>
</html>
