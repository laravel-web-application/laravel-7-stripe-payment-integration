<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');

Route::get('stripe2', 'StripePaymentController@index');
Route::post('store', 'StripePaymentController@store');

Route::get('payment', 'StripePaymentController@index2');
Route::post('charge', 'StripePaymentController@charge');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
