<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Stripe Integration
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-stripe-payment-integration.git`
2. Go inside the folder: `cd laravel-7-stripe-payment-integration`
3. Run `composer install`
4. Run `php artisan key:generate`
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000

Name: Test

Number: 4242 4242 4242 4242

CSV: 123

Expiration Month: 12

Expiration Year: 2024

You can use the following card details for testing purpose, if all this is not enough then you can [test more card numbers](https://stripe.com/docs/testing) 
and other information to make sure your integration works as planned.

|Number	| Brand	| CVC	|Date|
| --- | --- | --- | --- |
|4242424242424242	| Visa	| Any 3 digits	| Any future date|
|4000056655665556	| Visa (debit)	| Any 3 digits	| Any future date|
|5555555555554444	| Mastercard	| Any 3 digits	| Any future date|
|2223003122003222	| Mastercard (2-series)	| Any 3 digits	| Any future date|
|5200828282828210	| Mastercard (debit)	| Any 3 digits	| Any future date|
|5105105105105100	| Mastercard (prepaid)	| Any 3 digits	| Any future date|
|378282246310005	| American Express	| Any 4 digits	| Any future date|
|371449635398431	| American Express	| Any 4 digits	| Any future date|
|6011111111111117	| Discover	| Any 3 digits	| Any future date|
|6011000990139424	| Discover	| Any 3 digits	| Any future date|
|3056930009020004	| Diners Club	| Any 3 digits	| Any future date|
|36227206271667	    | Diners Club (14 digit card)	| Any 3 digits	| Any future date|
|3566002020360505	| JCB	| Any 3 digits	| Any future date|
|6200000000000005	| UnionPay	| Any 3 digits	| Any future date|
