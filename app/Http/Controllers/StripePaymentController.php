<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Omnipay\Omnipay;
use Stripe\Charge;
use Stripe\Stripe;

class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        Charge::create([
            "amount" => 212 * 100,
            "currency" => "USD",
            "source" => $request->stripeToken,
            "description" => "Test Stripe payment from hendisantika.com."
        ]);

        Session::flash('success', 'Payment successful!');

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('stripe2');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $stripe = Stripe::charges()->create([
            'source' => $request->get('tokenId'),
            'currency' => 'USD',
            'amount' => $request->get('amount') * 100
        ]);
        return $stripe;
//        Stripe::setApiKey(env('STRIPE_SECRET'));
//        Charge::create([
//            "amount" => $request->get('amount') * 100,
//            "currency" => "USD",
//            "source" => $request->get('tokenId'),
//            "description" => "Test payment from hendisantika.com"
//        ]);
//
//        Session::flash('success', 'Payment successful!');
//
//        return back();
    }

    public function paymentProcess(Request $request)
    {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
//        Stripe::setApiKey("STRIPE_SECRET");
        Stripe::setApiKey(env('STRIPE_SECRET'));

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        Charge::create([
            'amount' => 200,
            'currency' => 'USD',
            'description' => 'Test payment from hendisantika.com',
            'source' => $token,
        ]);

        Session::flash('success', 'Payment successful!');

        return back();
    }

    public function index2()
    {
        return view('payment');
    }

    public function charge(Request $request)
    {
        if ($request->input('stripeToken')) {

            $gateway = Omnipay::create('Stripe');
            $gateway->setApiKey(env('STRIPE_KEY'));

            $token = $request->input('stripeToken');

            $response = $gateway->purchase([
                'amount' => $request->input('amount'),
                'currency' => env('STRIPE_CURRENCY'),
                'token' => $token,
            ])->send();

            if ($response->isSuccessful()) {
                // payment was successful: insert transaction data into the database
                $arr_payment_data = $response->getData();

                $isPaymentExist = Payment::where('payment_id', $arr_payment_data['id'])->first();

                if (!$isPaymentExist) {
                    $payment = new Payment;
                    $payment->payment_id = $arr_payment_data['id'];
                    $payment->payer_email = $request->input('email');
                    $payment->amount = $arr_payment_data['amount'] / 100;
                    $payment->currency = env('STRIPE_CURRENCY');
                    $payment->payment_status = $arr_payment_data['status'];
                    $payment->save();
                }

                return "Payment is successful. Your payment id is: " . $arr_payment_data['id'];
            } else {
                // payment failed: display message to customer
                return $response->getMessage();
            }
        }
    }
}
